/**
 * 
 */
package com.babel.entities.user;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.babel.Application;
import com.babel.error.RestException;
import com.babel.login.LoginControllerTest;
import com.babel.utils.Constants;
import com.babel.utils.test.ConstantsTest;
import com.babel.utils.test.CustomHttpServletRequest;

/**
 * @author com.babel
 * User Controller Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class, LoginControllerTest.class})
@WebAppConfiguration
public class UserControllerTest {

	@Autowired
	private Constants constants;

	@Autowired
	private ConstantsTest constantsTest;

	@Autowired
	private LoginControllerTest loginControllerTest;

	@Autowired
	private UserController userController;

	// Set User to use
	@Before
	public void setUp() throws Exception {
		loginControllerTest.setUp();
		loginControllerTest.loginOKTest();
	}

	@Test
	public void getUser() throws RestException {
		// Create Request Header
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		CustomHttpServletRequest mockRequest = new CustomHttpServletRequest(request);
		mockRequest.putHeader(constants.jwtHeader, constantsTest.userSession.getToken());

		// Get User from session
		UserDTOResponse user = userController.getUser(mockRequest);

		// Check everything is ok
		Assert.assertNotNull(user.getUuid());
		Assert.assertNotNull(user.getName());
		Assert.assertNotNull(user.getSurnames());
		Assert.assertNotNull(user.getUsername());
		Assert.assertNotNull(user.getEmail());
	}
}
