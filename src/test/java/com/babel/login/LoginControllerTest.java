/**
 * 
 */
package com.babel.login;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.babel.Application;
import com.babel.security.entities.AuthenticatedUser;
import com.babel.security.entities.AuthenticatingUser;
import com.babel.utils.test.ConstantsTest;

/**
 * @author com.babel
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class LoginControllerTest {
	@Autowired
	private ConstantsTest constantsTest;

	@Autowired
	private LoginController loginController;

	private AuthenticatingUser authenticatingUser;

	@Before
	public void setUp() {
		// Set User to authenticate
		authenticatingUser = new AuthenticatingUser();
		authenticatingUser.setUsername("goico");
		authenticatingUser.setPassword("aaaaaa");
	}

	@Test
	public void loginOKTest() throws Exception {
		AuthenticatedUser user = loginController.login(authenticatingUser);

		// Check that user is not null
		Assert.assertNotNull(user);
		Assert.assertNotEquals("", user.getToken());

		// Save user in session
		constantsTest.userSession = user;
	}
}
