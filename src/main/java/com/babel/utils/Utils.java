/**
 * 
 */
package com.babel.utils;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.babel.enums.RestExceptionE;
import com.babel.error.RestException;

/**
 * Common file for common functions
 * 
 * @author com.babel
 */
@Component
public class Utils implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6684428657087250594L;

    private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    /**
     * Function to know if a List is not null and not empty
     * 
     * @param list List
     * @return boolean
     */
    public boolean isNotNullANDNotUndefined(List<?> list) {
        return list != null && !list.isEmpty() ? true : false;
    }

    /**
     * Function to know if a string is not null and not empty
     * 
     * @param string String
     * @return boolean
     */
    public boolean isNotNullANDEmptyString(String string) {
        return string != null && !"".equals(string) ? true : false;
    }

    /**
     * Function to convert String to Date
     * 
     * @param date Date
     * @return Date
     * @throws ParseException ParseException
     */
    public Date stringToDate(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return isNotNullANDEmptyString(date) ? formatter.parse(date) : null;
    }

    /**
     * Function to convert Date to String
     * 
     * @param date Date
     * @return String
     */
    public String dateToString(Date date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return date != null ? formatter.format(date) : null;
    }

    /**
     * Function to convert String to Timestamp
     * 
     * @param date Date
     * @return Date
     * @throws ParseException ParseException
     */
    public Date stringToTimestamp(String date) throws ParseException {
        return isNotNullANDEmptyString(date) ? new Date(Long.parseLong(date)) : null;
    }

    /**
     * Function to generate random number
     * 
     * @return String
     */
    public String randomString() {
        int len = 10;
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    /**
     * Function to create Calendar from date
     * 
     * @param date Date
     * @return Calendar
     */
    public Calendar dateToCalendarTruncateTime(Date date) {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    /**
     * Function to check two passwords
     * 
     * @param pass1 String
     * @param pass2 String
     * @return Boolean
     */
    public Boolean checkPasswords(String pass1, String pass2) {
        return "".equals(pass1) || "".equals(pass2) || !pass1.equals(pass2) ? false : true;
    }

    /**
     * Function to valid file image
     * 
     * @param file MultipartFile
     * @return Boolean
     * @throws RestException RestException
     */
    public Boolean checkFileImage(MultipartFile file) throws RestException {
        // Check size file
        if (!checkFileSize(file)) {
            throw new RestException(RestExceptionE.FILE_SIZE_NOT_SUPPORTED);
        }

        // Check type image
        if (!checkImageSupport(file.getContentType())) {
            throw new RestException(RestExceptionE.FILE_TYPE_NOT_SUPPORTED);
        }

        return true;
    }

    /**
     * Function to check valid type images
     * 
     * @param contentType String
     * @return Boolean
     */
    public Boolean checkImageSupport(String contentType) {
        return contentType.matches("image/png") || contentType.matches("image/jpeg") || contentType.matches("image/jpg") ? true : false;
    }

    /**
     * Function to check size file
     * 
     * @param file MultipartFile
     * @return Boolean
     */
    public Boolean checkFileSize(MultipartFile file) {
        return file.isEmpty() || file.getSize() > 5000000 ? false : true;
    }

    /**
     * Function to create converter to skip null parameters
     * 
     * @return Converter
     */
    public Converter<String, String> skipNullConverter() {
        return new Converter<String, String>() {
            @Override
            public String convert(MappingContext<String, String> context) {
                return context.getSource() == null ? context.getDestination() : context.getSource();
            }
        };
    }

    /**
     * Function to create convert to pass date to string
     * 
     * @return AbstractConverter
     */
    public AbstractConverter<Date, String> convertDateToString() {
        return new AbstractConverter<Date, String>() {
            @Override
            protected String convert(Date source) {
                return dateToString(source);
            }
        };
    }

    /**
     * Function to create converter to pass string to date
     * 
     * @return AbstractConverter
     */
    public AbstractConverter<String, Date> convertStringToDate() {
        return new AbstractConverter<String, Date>() {
            @Override
            protected Date convert(String source) {
                try {
                    return stringToDate(source);
                } catch (ParseException e) {
                    LOGGER.error(e);
                }
                return null;
            }
        };
    }

    /**
     * Function to pass a string to Sha1
     * 
     * @param input String
     * @return String
     */
    public String toSha1(String input) {
        MessageDigest md;
        byte[] buffer, digest;
        String hash = "";

        try {
            buffer = input.getBytes();
            md = MessageDigest.getInstance("SHA1");
            md.update(buffer);
            digest = md.digest();

            for (byte aux : digest) {
                int b = aux & 0xff;
                if (Integer.toHexString(b).length() == 1)
                    hash += "0";
                hash += Integer.toHexString(b);
            }
        } catch (Exception e) {
            LOGGER.error("Could not hash String: " + input, e);
        }

        return hash;
    }
}
