/**
 * 
 */
package com.babel.utils;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Component for all global constants
 * 
 * @author com.babel
 */
@Component
public class Constants implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -3831540713382731171L;

    private static final Logger LOGGER = Logger.getLogger(Constants.class.getName());

    @Value("${jwt.header}")
    public String jwtHeader;

    @Value("${jwt.secret}")
    public String jwtSecret;

    @Value("${jwt.expiration}")
    public Long jwtExpiration;

    @Value("${spring.data.rest.basePath}")
    public String basePath;

    @Value("${spring.contextPath}")
    public String contextPath;

    /**
     * Method that execute after Spring start, so we can access all repositories
     * from database
     */
    @PostConstruct
    public void initConstantsDB() {
        LOGGER.info("Load all statis tables from Database");
    }
}
