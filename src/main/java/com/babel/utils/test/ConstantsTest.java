/**
 * 
 */
package com.babel.utils.test;

import org.springframework.stereotype.Component;

import com.babel.security.entities.AuthenticatedUser;

/**
 * @author com.babel
 *
 */
@Component
public class ConstantsTest {
    // Constant for save the user
    public AuthenticatedUser userSession;
}
