/**
 * 
 */
package com.babel.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.babel.enums.RestExceptionE;
import com.babel.error.RestException;
import com.babel.security.JwtAuthenticationEntryPoint;
import com.babel.security.JwtAuthenticationTokenFilter;
import com.babel.security.JwtPasswordEncoder;
import com.babel.security.JwtUserDetailsServiceImpl;

/**
 * Spring security configuration
 * 
 * @author com.babel
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = Logger.getLogger(WebSecurityConfig.class.getSimpleName());

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private JwtUserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtPasswordEncoder passwordEncoder;

    /**
     * Define Authentication Manager 
     * @return JwtAuthenticationTokenFilter
     * @throws RestException If wrong user
     */
    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws RestException {
        JwtAuthenticationTokenFilter authenticationTokenFilter = new JwtAuthenticationTokenFilter();

        try {
            authenticationTokenFilter.setAuthenticationManager(authenticationManagerBean());
        } catch (Exception e) {
            LOGGER.error(e);
            throw new RestException(RestExceptionE.AUTHENTICATION_TOKEN_ERROR);
        }

        return authenticationTokenFilter;
    }

    /**
     * 
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws RestException {
        AuthenticationManager authenticationManager = null;
        try {
            authenticationManager = super.authenticationManagerBean();
        } catch (Exception e) {
            LOGGER.error(e);
            throw new RestException(RestExceptionE.AUTHENTICATION_TOKEN_ERROR);
        }

        return authenticationManager;
    }

    /**
     * Configure the authentication 
     * @param authenticationManagerBuilder authenticationManagerBuilder 
     * @throws RestException If wrong user
     */
    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder)
            throws RestException {
        try {
            authenticationManagerBuilder.userDetailsService(this.userDetailsService).passwordEncoder(passwordEncoder);
        } catch (Exception e) {
            LOGGER.error(e);
            throw new RestException(RestExceptionE.AUTHENTICATION_TOKEN_ERROR);
        }
    }

    /**
     * Function to define the configuration from the Spring Security
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws RestException {
        try {
            httpSecurity.csrf().disable().exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .authorizeRequests()
                        .antMatchers("/api/users/**").authenticated()
                        .antMatchers("/api/repositories/**").authenticated()
                        .antMatchers("/**").permitAll();

            httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

            httpSecurity.headers().cacheControl();
        } catch (Exception e) {
            LOGGER.error(e);
            throw new RestException(RestExceptionE.AUTHENTICATION_TOKEN_ERROR);
        }
    }
}
