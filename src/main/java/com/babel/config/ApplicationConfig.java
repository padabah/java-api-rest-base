/**
 * 
 */
package com.babel.config;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * Application configuration
 * 
 * @author com.babel
 */
@Configuration
public class ApplicationConfig {
    private static final Logger LOGGER = Logger.getLogger(ApplicationConfig.class.getName());

    @Value("${spring.locale.default}")
    private String locale;

    @Value("${spring.messages.basename}")
    private String basename;

    /**
     * Message Resource declaration.
     *
     * @return MessageRessource
     */
    @Bean
    public MessageSource messageSource() {
        LOGGER.info("Set Locale to: " + locale);
        Locale.setDefault(new Locale(locale));

        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasename(basename);
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }
}
