package com.babel.enums;

import org.springframework.http.HttpStatus;

/**
 * Numerators for all Rest Exceptions that we are going to send
 * 
 * @author com.babel
 */
public enum RestExceptionE {

    USER_NOT_EXISTS(1000, HttpStatus.BAD_REQUEST, "user.not.exists"), UUID_NOT_EXISTS(1001, HttpStatus.BAD_REQUEST,
            "uuid.not.exists"), EMAIL_NOT_ACTIVATE(1002, HttpStatus.BAD_REQUEST, "email.not.activate"), PARAM_INCORRECT(
                    1003, HttpStatus.BAD_REQUEST, "param.incorrect"), EMAIL_ALREADY_ACTIVATED(1004,
                            HttpStatus.BAD_REQUEST, "email.already.activated"), AUTHENTICATION_TOKEN_ERROR(1005,
                                    HttpStatus.BAD_REQUEST, "authentication.token.error"), FILE_SIZE_NOT_SUPPORTED(2001,
                                            HttpStatus.BAD_REQUEST, "file.size.not.supported"), FILE_TYPE_NOT_SUPPORTED(
                                                    2002, HttpStatus.BAD_REQUEST, "file.type.not.supported"),;

    private Integer id;
    private HttpStatus httpStatus;
    private String message;

    private RestExceptionE(final Integer pId, final HttpStatus pHttpStatus, final String pMessage) {
        this.id = pId;
        this.httpStatus = pHttpStatus;
        this.message = pMessage;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the httpStatus
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
}