/**
 * 
 */
package com.babel.converters;

import javax.persistence.AttributeConverter;

/**
 * Class for change Boolean value to Database value
 * 
 * @author com.babel
 */
public class BooleanYNConverter implements AttributeConverter<Boolean, String> {

    public static final String TRUE = "Y";
    public static final String FALSE = "N";

    /**
     * Change Boolean to Database value
     */
    @Override
    public String convertToDatabaseColumn(Boolean attribute) {
        if (Boolean.TRUE.equals(attribute)) {
            return BooleanYNConverter.TRUE;
        } else {
            return BooleanYNConverter.FALSE;
        }
    }

    /**
     * Change Database value to Boolean
     */
    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        if (BooleanYNConverter.TRUE.equals(dbData)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
