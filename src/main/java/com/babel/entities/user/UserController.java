/**
 * 
 */
package com.babel.entities.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.babel.enums.RestExceptionE;
import com.babel.error.RestException;
import com.babel.security.JwtPasswordEncoder;
import com.babel.security.JwtTokenUtil;
import com.babel.utils.Constants;
import com.babel.utils.Utils;
import com.fasterxml.uuid.Generators;

/**
 * @author com.babel User Controller
 */
@Controller
@RequestMapping(value = "/api/users")
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class.getName());

    @Autowired
    private Constants constants;

    @Autowired
    private Utils utils;

    @Autowired
    private JwtPasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserRepository userRepository;

    /**
     * Create new user
     * 
     * @param userRequest UserDTORequest
     * @throws RestException RestException
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public void newUser(@Valid @RequestBody UserDTORequest userRequest) throws RestException {
        // Create user
        LOGGER.info("Create user: " + userRequest.getName());
        ModelMapper modelMapper = new ModelMapper();

        // Map Request user to user entity
        User user = modelMapper.map(userRequest, User.class);

        // Define entity values
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        user.setUuid(Generators.timeBasedGenerator().generate().toString());
        user.setCreationDate(new Date());
        user.setIsDeleted(false);

        // Save User on database
        userRepository.save(user);
        LOGGER.info("User created correctly");
    }

    /**
     * Update user
     * 
     * @param userRequest UserDTORequest
     * @param request HttpServletRequest
     * @throws RestException RestException
     */
    @RequestMapping(value = "/profile", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void updateUser(@Valid @RequestBody UserDTORequest userRequest, HttpServletRequest request) throws RestException {
        // Update user
        LOGGER.info("Update user: " + userRequest.getName());
        ModelMapper modelMapper = new ModelMapper();

        // Get user from session
        String token = request.getHeader(constants.jwtHeader);
        String uuid = jwtTokenUtil.getUuidFromToken(token);

        // Get user from database
        User userBBDD = userRepository.findByUuid(uuid);

        // Check if exists
        if (userBBDD != null) {
            // Skip Null values
            modelMapper.addConverter(utils.skipNullConverter());
            modelMapper.map(userRequest, userBBDD);
            userBBDD.setModifyDate(new Date());

            // Update user
            userRepository.save(userBBDD);
            LOGGER.info("User update correctly");
        } else
            throw new RestException(RestExceptionE.USER_NOT_EXISTS);
    }

    /**
     * Get detail user
     * 
     * @param request HttpServletRequest
     * @return UserDTOResponse
     * @throws RestException RestException
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public UserDTOResponse getUser(HttpServletRequest request) throws RestException {
        // Get user
        LOGGER.info("Get user data");
        ModelMapper modelMapper = new ModelMapper();

        // Get user from session
        String token = request.getHeader(constants.jwtHeader);
        String uuid = jwtTokenUtil.getUuidFromToken(token);

        // Get user from database
        User userBBDD = userRepository.findByUuid(uuid);

        // Check if exists
        if (userBBDD != null) {
            // Map result
            return modelMapper.map(userBBDD, UserDTOResponse.class);
        } else
            throw new RestException(RestExceptionE.USER_NOT_EXISTS);
    }

    /**
     * Delete user
     * 
     * @param request HttpServletRequest
     * @throws RestException RestException
     */
    @RequestMapping(value = "/user", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteUser(HttpServletRequest request) throws RestException {
        // Delete user
        LOGGER.info("Delete user");

        // Get user from session
        String token = request.getHeader(constants.jwtHeader);
        String uuid = jwtTokenUtil.getUuidFromToken(token);

        // Get user from database
        User userBBDD = userRepository.findByUuid(uuid);

        // Check if exists
        if (userBBDD != null) {
            // Delete
            userBBDD.setDeleteDate(new Date());
            userBBDD.setIsDeleted(true);
            userRepository.save(userBBDD);
        } else
            throw new RestException(RestExceptionE.USER_NOT_EXISTS);
    }
}
