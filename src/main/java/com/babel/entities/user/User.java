/**
 * 
 */
package com.babel.entities.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.babel.historic.HistoricEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

/**
 * @author com.babel User entity
 */
@Entity
@Table(name = "app_user")
public class User extends HistoricEntity {

    /**
     * 
     */
    private static final long serialVersionUID = 5036271324956000101L;

    @Id
    @Column(name = "user_uuid", nullable = false, length = 36)
    @Getter
    @Setter
    private String uuid;

    @Column(name = "name", nullable = false, length = 50)
    @Getter
    @Setter
    private String name;

    @Column(name = "surnames", nullable = true, length = 50)
    @Getter
    @Setter
    private String surnames;

    @Column(name = "username", nullable = false, length = 50)
    @Getter
    @Setter
    private String username;

    @Column(name = "email", nullable = false, length = 250)
    @Getter
    @Setter
    private String email;

    @Column(name = "phone", nullable = false, length = 15)
    @Getter
    @Setter
    private String phone;

    @JsonIgnore
    @Column(name = "password", nullable = false, length = 100)
    @Getter
    @Setter
    private String password;

    @Column(name = "address", nullable = true, length = 250)
    @Getter
    @Setter
    private String address;

    @Column(name = "zip_code", nullable = true, length = 10)
    @Getter
    @Setter
    private String zipcode;

    @Column(name = "country", nullable = true, length = 250)
    @Getter
    @Setter
    private String country;

    @Column(name = "email_activation_date", nullable = true)
    @Getter
    @Setter
    private Date emailActivationDate;

    @JsonIgnore
    @Column(name = "session_token", nullable = true, length = 50)
    @Getter
    @Setter
    private String sessionToken;
}
