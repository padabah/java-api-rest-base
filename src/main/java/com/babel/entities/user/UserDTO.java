/**
 * 
 */
package com.babel.entities.user;

import java.io.Serializable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author com.babel Object DTO to receive and send User as we want
 */
public class UserDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7698367082184637536L;

    @NotEmpty
    @Length(max = 50)
    @Getter
    @Setter
    private String name;

    @Length(max = 50)
    @Getter
    @Setter
    private String surnames;

    @NotEmpty
    @Length(max = 50)
    @Getter
    @Setter
    private String username;

    @NotEmpty
    @Length(max = 250)
    @Getter
    @Setter
    private String email;

    @NotEmpty
    @Length(max = 15)
    @Getter
    @Setter
    private String phone;

    @Length(max = 250)
    @Getter
    @Setter
    private String address;

    @Length(max = 10)
    @Getter
    @Setter
    private String zipcode;

    @Length(max = 250)
    @Getter
    @Setter
    private String country;
}
