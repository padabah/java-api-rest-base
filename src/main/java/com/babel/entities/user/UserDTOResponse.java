/**
 * 
 */
package com.babel.entities.user;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author com.babel Object DTO to send User as we want
 */
public class UserDTOResponse extends UserDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7698367082184637536L;

    @Getter
    @Setter
    private String uuid;
}
