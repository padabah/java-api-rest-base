/**
 * 
 */
package com.babel.entities.user;

import java.io.Serializable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author com.babel Object DTO to receive User as we want
 */
public class UserDTORequest extends UserDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7698367082184637536L;

    @NotEmpty
    @Length(max = 50)
    @Getter
    @Setter
    private String password;
}
