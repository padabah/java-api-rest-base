/**
 * 
 */
package com.babel.entities.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * User repository
 * 
 * @author com.babel
 */
@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends CrudRepository<User, String> {

    /**
     * Find user by username
     * 
     * @param username String
     * @return User
     */
    User findByUsername(String username);

    /**
     * Find user by email
     * 
     * @param email String
     * @return User
     */
    User findByEmail(String email);

    /**
     * Find user by uuid
     * 
     * @param uuid String
     * @return User
     */
    User findByUuid(String uuid);
}
