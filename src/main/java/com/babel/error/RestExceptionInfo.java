package com.babel.error;

import org.springframework.http.HttpStatus;

/**
 * @author com.babel
 */
public class RestExceptionInfo {

    private final HttpStatus status;
    private final int code;
    private final String message;
    private final String developerMessage;
    private final String moreInfoUrl;
    private final Throwable throwable;

    public RestExceptionInfo(HttpStatus status, int code, String message, String developerMessage, String moreInfoUrl,
            Throwable throwable) {
        if (status == null) {
            throw new NullPointerException("HttpStatus argument cannot be null.");
        }
        this.status = status;
        this.code = code;
        this.message = message;
        this.developerMessage = developerMessage;
        this.moreInfoUrl = moreInfoUrl;
        this.throwable = throwable;
    }

    public RestExceptionInfo(HttpStatus status, int code, String message) {
        if (status == null) {
            throw new NullPointerException("HttpStatus argument cannot be null.");
        }
        this.status = status;
        this.code = code;
        this.message = message;
        this.developerMessage = null;
        this.moreInfoUrl = null;
        this.throwable = null;
    }

    /**
     * @return the status
     */
    public HttpStatus getStatus() {
        return status;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the developerMessage
     */
    public String getDeveloperMessage() {
        return developerMessage;
    }

    /**
     * @return the moreInfoUrl
     */
    public String getMoreInfoUrl() {
        return moreInfoUrl;
    }

    /**
     * @return the throwable
     */
    public Throwable getThrowable() {
        return throwable;
    }
}