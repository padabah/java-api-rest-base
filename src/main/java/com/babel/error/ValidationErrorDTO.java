package com.babel.error;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author com.babel
 */
public class ValidationErrorDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3078012659335988010L;

    private List<FieldErrorDTO> fieldErrors = new ArrayList<>();

    public void addFieldError(String path, String message) {
        FieldErrorDTO error = new FieldErrorDTO(path, message);
        fieldErrors.add(error);
    }

    /**
     * @return the fieldErrors
     */
    public List<FieldErrorDTO> getFieldErrors() {
        return fieldErrors;
    }

    /**
     * @param fieldErrors
     *            the fieldErrors to set
     */
    public void setFieldErrors(List<FieldErrorDTO> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}