package com.babel.error;

import java.io.Serializable;

/**
 * Class for Field error
 * 
 * @author com.babel
 */
public class FieldErrorDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1508068555762684697L;

    private String field;
    private String message;

    public FieldErrorDTO(String field, String message) {
        this.field = field;
        this.message = message;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field
     *            the field to set
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}