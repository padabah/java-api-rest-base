package com.babel.error;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author com.babel
 */
@ControllerAdvice
public class RestErrorHandler {
    private static final Logger LOGGER = Logger.getLogger(RestErrorHandler.class.getName());
    private MessageSource messageSource;

    @Autowired
    public RestErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * Process Field Errors
     * 
     * @param fieldErrors List
     * @return ValidationErrorDTO
     */
    private ValidationErrorDTO processFieldErrors(List<FieldError> fieldErrors) {
        ValidationErrorDTO dto = new ValidationErrorDTO();

        for (FieldError fieldError : fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }

        return dto;
    }

    /**
     * Resolve Localised Errors
     * 
     * @param fieldError FieldError
     * @return String
     */
    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale = LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);

        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }

        return localizedErrorMessage;
    }

    /**
     * MethodArgumentNotValidException
     * 
     * @param ex MethodArgumentNotValidException
     * @return ValidationErrorDTO
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDTO processValidationError(MethodArgumentNotValidException ex) {
        LOGGER.error(ex.getMessage());
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();

        return processFieldErrors(fieldErrors);
    }

    /**
     * UnexpectedRollbackException
     * 
     * @param ex UnexpectedRollbackException
     * @return ValidationErrorDTO
     */
    @ExceptionHandler(UnexpectedRollbackException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDTO processValidationError(UnexpectedRollbackException ex) {
        LOGGER.error(ex.getMessage());
        ValidationErrorDTO dto = new ValidationErrorDTO();
        dto.addFieldError("", ex.getMostSpecificCause().toString());
        return dto;
    }

    /**
     * EntityNotFoundException
     * 
     * @param ex EntityNotFoundException
     * @return ValidationErrorDTO
     */
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDTO processValidationError(EntityNotFoundException ex) {
        LOGGER.error(ex.getMessage());
        ValidationErrorDTO dto = new ValidationErrorDTO();
        dto.addFieldError("", ex.getMessage());
        return dto;
    }

    /**
     * JpaObjectRetrievalFailureException
     * 
     * @param ex JpaObjectRetrievalFailureException
     * @return ValidationErrorDTO
     */
    @ExceptionHandler(JpaObjectRetrievalFailureException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDTO processValidationError(JpaObjectRetrievalFailureException ex) {
        LOGGER.error(ex.getMessage());
        ValidationErrorDTO dto = new ValidationErrorDTO();
        dto.addFieldError("", ex.getMessage());
        return dto;
    }

    /**
     * RestException
     * 
     * @param response HttpServletResponse
     * @param ex RestException
     * @return RestExceptionInfo
     */
    @ExceptionHandler(RestException.class)
    @ResponseBody
    public RestExceptionInfo exceptionHandler(HttpServletResponse response, RestException ex) {
        LOGGER.error(ex.getInfo().name() + ": " + ex.getInfo().getMessage());
        response.setStatus(ex.getInfo().getHttpStatus().value());
        return new RestExceptionInfo(ex.getInfo().getHttpStatus(), ex.getInfo().getId(), ex.getInfo().getMessage());
    }
}