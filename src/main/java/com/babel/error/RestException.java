package com.babel.error;

import com.babel.enums.RestExceptionE;

/**
 * @author com.babel
 */
public class RestException extends Exception {

    private static final long serialVersionUID = -6234046511081229495L;

    private final RestExceptionE info;

    public RestException(RestExceptionE pInfo) {
        super();
        this.info = pInfo;
    }

    /**
     * @return the info
     */
    public RestExceptionE getInfo() {
        return info;
    }
}