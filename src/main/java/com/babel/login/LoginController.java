/**
 * 
 */
package com.babel.login;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.babel.entities.user.User;
import com.babel.entities.user.UserRepository;
import com.babel.enums.RestExceptionE;
import com.babel.error.RestException;
import com.babel.security.JwtTokenUtil;
import com.babel.security.JwtUser;
import com.babel.security.JwtUserDetailsServiceImpl;
import com.babel.security.entities.AuthenticatedUser;
import com.babel.security.entities.AuthenticatingUser;
import com.babel.utils.Constants;

/**
 * @author com.babel
 */
@Controller
public class LoginController {
    @Autowired
    private Constants constants;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());

    /**
     * Login
     * 
     * @param authenticatingUser AuthenticatingUser
     * @return AuthenticatedUser
     * @throws RestException RestException
     */
    @RequestMapping(value = "/api/login", method = RequestMethod.POST)
    @ResponseBody
    public AuthenticatedUser login(@Valid @RequestBody AuthenticatingUser authenticatingUser) throws RestException {
        AuthenticatedUser authenticatedUser = new AuthenticatedUser();
        final JwtUser userDetails = userDetailsService.loadUserByUsername(authenticatingUser.getUsername());

        // Check if the user exists in the data base
        User userBBDD = userRepository.findOne(userDetails.getId());
        if (userBBDD != null) {
            Date now = new Date();

            // Check if the user is activated
            if (userBBDD.getEmailActivationDate() != null && now.after(userBBDD.getEmailActivationDate())) {
                LOGGER.info("User is logged correctly");

                // Create the user for Spring security
                final Authentication authentication = authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(authenticatingUser.getUsername(),
                                authenticatingUser.getPassword()));

                // Create security context
                SecurityContextHolder.getContext().setAuthentication(authentication);
                final String token = jwtTokenUtil.generateToken(userDetails);

                authenticatedUser.setToken(token);
                authenticatedUser.setName(userBBDD.getName());
                authenticatedUser.setUuid(userBBDD.getUuid());

                // Update user
                userBBDD.setSessionToken(token);
                userRepository.save(userBBDD);
            } else {
                throw new RestException(RestExceptionE.EMAIL_NOT_ACTIVATE);
            }
        } else {
            throw new RestException(RestExceptionE.USER_NOT_EXISTS);
        }

        return authenticatedUser;
    }

    /**
     * Logout
     * 
     * @param request HttpServletRequest
     * @throws RestException RestException
     */
    @RequestMapping(value = "/api/users/logout", method = RequestMethod.POST)
    @ResponseBody
    public void logout(HttpServletRequest request) throws RestException {
        String token = request.getHeader(constants.jwtHeader);
        String uuid = jwtTokenUtil.getUuidFromToken(token);

        // Check the user in the session
        if (uuid != null) {
            User userBBDD = userRepository.findOne(uuid);

            // Check if the user exists
            if (userBBDD != null) {
                LOGGER.info("User is logout correctly");
                userBBDD.setSessionToken(null);
                userRepository.save(userBBDD);
            } else {
                throw new RestException(RestExceptionE.USER_NOT_EXISTS);
            }
        } else {
            throw new RestException(RestExceptionE.UUID_NOT_EXISTS);
        }
    }
}
