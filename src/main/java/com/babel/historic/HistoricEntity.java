package com.babel.historic;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

import com.babel.converters.BooleanYNConverter;

import lombok.Getter;
import lombok.Setter;

/**
 * Superclass for common columns
 * 
 * @author com.babel
 */
@MappedSuperclass
public class HistoricEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5391236433720194584L;

    @Column(name = "CREATION_DATE")
    @Getter
    @Setter
    private Date creationDate;

    @Column(name = "MODIFY_DATE")
    @Getter
    @Setter
    private Date modifyDate;

    @Column(name = "DELETE_DATE")
    @Getter
    @Setter
    private Date deleteDate;

    @Column(name = "IS_DELETED", nullable = false)
    @Convert(converter = BooleanYNConverter.class)
    private Boolean isDeleted;

    /**
     * @return the isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted
     *            the isDeleted to set
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}