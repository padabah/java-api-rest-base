package com.babel.security.entities;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author com.babel
 */
public class AuthenticatedUser implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1485801499496477775L;

    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    private String uuid;

    @Getter
    @Setter
    private String name;
}