package com.babel.security.entities;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author com.babel
 */
public class AuthenticatingUser implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1752054058945622783L;

    @NotEmpty
    @Getter
    @Setter
    private String username;

    @NotEmpty
    @Getter
    @Setter
    private String password;
}