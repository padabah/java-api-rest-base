package com.babel.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.babel.entities.user.User;
import com.babel.entities.user.UserRepository;

/**
 * JwtUserDetailsServiceImpl
 * 
 * @author com.babel
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private static final Logger LOGGER = Logger.getLogger(JwtUserDetailsServiceImpl.class.getName());

    @Autowired
    private UserRepository userRepository;

    /**
     * Login User by username or email
     */
    @Override
    public JwtUser loadUserByUsername(String username) {
        LOGGER.info("Authenticate user by: " + username);
        // Find by username
        User user = userRepository.findByUsername(username);

        // Check if exists
        if (user == null) {
            // Find by email
            user = userRepository.findByEmail(username);

            if (user == null) {
                throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
            } else {
                return JwtUserFactory.create(user);
            }
        } else {
            return JwtUserFactory.create(user);
        }
    }
}