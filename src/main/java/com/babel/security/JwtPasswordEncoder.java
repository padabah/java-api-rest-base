/**
 * 
 */
package com.babel.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author carlos.goicoechea.local
 *
 */
@Component
public class JwtPasswordEncoder extends BCryptPasswordEncoder {

}
