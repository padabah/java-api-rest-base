package com.babel.security;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.babel.entities.user.User;
import com.babel.entities.user.UserRepository;
import com.babel.utils.Constants;
import com.babel.utils.Utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author com.babel
 */
@Component
public class JwtTokenUtil implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8813316467616487776L;

    private static final Logger LOGGER = Logger.getLogger(JwtTokenUtil.class.getName());
    private static final String ERROR = "Token error";

    @Autowired
    private Constants constants;

    @Autowired
    private Utils utils;

    @Autowired
    private UserRepository userRepository;

    private static final String CLAIM_KEY_UUID = "uuid";
    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";

    /**
     * getClaimsFromToken
     * 
     * @param token String
     * @return Claims
     */
    private Claims getClaimsFromToken(String token) {
        Claims claims = null;

        if (utils.isNotNullANDEmptyString(token)) {
            try {
                claims = Jwts.parser().setSigningKey(constants.jwtSecret).parseClaimsJws(token).getBody();
            } catch (Exception e) {
                LOGGER.error(ERROR, e);
            }
        }

        return claims;
    }

    /**
     * generateExpirationDate
     * 
     * @return Date
     */
    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + constants.jwtExpiration * 1000);
    }

    /**
     * generateToken
     * 
     * @param claims Map
     * @return String
     */
    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate()).signWith(SignatureAlgorithm.HS512, constants.jwtSecret).compact();
    }

    /**
     * isTokenExpired
     * 
     * @param token String
     * @return Boolean
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * getUuidFromToken
     * 
     * @param token String
     * @return String
     */
    public String getUuidFromToken(String token) {
        String id = null;

        if (utils.isNotNullANDEmptyString(token)) {
            try {
                final Claims claims = getClaimsFromToken(token);
                id = (String) claims.get(CLAIM_KEY_UUID);
            } catch (Exception e) {
                LOGGER.error(ERROR, e);
            }
        }

        return id;
    }

    /**
     * getUsernameFromToken
     * 
     * @param token String
     * @return String
     */
    public String getUsernameFromToken(String token) {
        String username = null;

        if (utils.isNotNullANDEmptyString(token)) {
            try {
                final Claims claims = getClaimsFromToken(token);
                username = claims.getSubject();
            } catch (Exception e) {
                LOGGER.error(ERROR, e);
            }
        }

        return username;
    }

    /**
     * getCreatedDateFromToken
     * 
     * @param token String
     * @return Date
     */
    public Date getCreatedDateFromToken(String token) {
        Date created = null;

        if (utils.isNotNullANDEmptyString(token)) {
            try {
                final Claims claims = getClaimsFromToken(token);
                created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
            } catch (Exception e) {
                LOGGER.error(ERROR, e);
            }
        }

        return created;
    }

    /**
     * getExpirationDateFromToken
     * 
     * @param token String
     * @return Date
     */
    public Date getExpirationDateFromToken(String token) {
        Date expiration = null;

        if (utils.isNotNullANDEmptyString(token)) {
            try {
                final Claims claims = getClaimsFromToken(token);
                expiration = claims.getExpiration();
            } catch (Exception e) {
                LOGGER.error(ERROR, e);
            }
        }

        return expiration;
    }

    /**
     * validateToken
     * 
     * @param token String
     * @param userDetails UserDetails
     * @return Boolean
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        JwtUser user = (JwtUser) userDetails;
        final String uuid = getUuidFromToken(token);

        Boolean valid = false;

        if (uuid != null) {
            User userBBDD = userRepository.findOne(uuid);

            if (userBBDD != null) {
                Date now = new Date();
                valid = token.equals(userBBDD.getSessionToken());
                valid &= uuid.equals(user.getId());
                valid &= !isTokenExpired(token);
                valid &= userBBDD.getEmailActivationDate() != null && now.after(userBBDD.getEmailActivationDate());
            }
        }

        return valid;
    }

    /**
     * generateToken
     * 
     * @param userDetails JwtUser
     * @return String
     */
    public String generateToken(JwtUser userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_UUID, userDetails.getId());
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(claims);
    }
}