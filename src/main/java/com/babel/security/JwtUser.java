package com.babel.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author com.babel
 */
public class JwtUser implements UserDetails {

    /**
     * 
     */
    private static final long serialVersionUID = 487337569845661019L;

    private final String id;
    private final String username;
    private final String firstname;
    private final String lastname;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorities;
    private final boolean enabled;

    /**
     * 
     * @param id id
     * @param username username
     * @param firstname firstname
     * @param lastname lastname
     * @param password password
     * @param authorities authorities
     * @param enabled enabled
     */
    public JwtUser(String id, String username, String firstname, String lastname, String password, Collection<? extends GrantedAuthority> authorities, boolean enabled) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.authorities = authorities;
        this.enabled = enabled;
    }

    /**
     * getId
     * 
     * @return String
     */
    @JsonIgnore
    public String getId() {
        return id;
    }

    /**
     * getUsername
     */
    @Override
    public String getUsername() {
        return username;
    }

    /**
     * isAccountNonExpired
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * isAccountNonLocked
     */
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * isCredentialsNonExpired
     */
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * getFirstname
     * 
     * @return String
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * getLastname
     * 
     * @return String
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * getPassword
     */
    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * getAuthorities
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    /**
     * isEnabled
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }
}