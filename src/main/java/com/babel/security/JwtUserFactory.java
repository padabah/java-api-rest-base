package com.babel.security;

import com.babel.entities.user.User;

/**
 * @author com.babel
 */
public class JwtUserFactory {
    private JwtUserFactory() {
    }

    /**
     * Create Jwt User
     * 
     * @param user User
     * @return JwtUser
     */
    public static JwtUser create(User user) {
        return new JwtUser(user.getUuid(), user.getUsername(), user.getName(), user.getSurnames(), user.getPassword(), null, true);
    }
}