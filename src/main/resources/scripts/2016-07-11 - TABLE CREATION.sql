CREATE USER 'java-api-rest'@'localhost' IDENTIFIED BY 'Babel1234';

CREATE TABLE app_user (
  user_uuid VARCHAR(36) NOT NULL PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  surnames VARCHAR(50),
  username VARCHAR(50) NOT NULL,
  email VARCHAR(250) NOT NULL,
  phone VARCHAR(15) NOT NULL,
  password VARCHAR(100) NOT NULL,
  address VARCHAR(250),
  zip_code VARCHAR(10),
  country VARCHAR(250),
  email_activation_date DATE,
  session_token VARCHAR(1000),
  creation_date DATE NOT NULL,
  modify_date DATE,
  delete_date DATE,
  is_deleted VARCHAR(1) DEFAULT 'N' NOT NULL
);