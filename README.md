# Java API Rest
## Introducción
Proyecto base Java que utiliza **Spring Boot App** + **MySQL** y está preparado para que funcione en la mayoría de servidores que tenemos en el CDM. Además cuenta con las siguientes funciones:

* **[Log4j]** - Gestión de Logs
* **[JUnit]** - Pruebas con JUnit e integración continua
* **[Maven]** - Gestión de dependencias con Maven y generación de War
* **[Spring JPA]** - Gestión de repositorios
* **[Spring Security]** - Gestión de la seguridad
* **[Jwt]** - Control de sesiones de los usuarios
* **[ModelMapper]** - Mapeador de objetos
* **[Sonar]** - Evaluación de código fuente
* **[JavaDoc]** - Generación de documentación

## Entorno

### Servidor

Como hemos comentado anteriormente esta aplicación está preparada para funcionar en la mayoría de servidores. Aunque en función del servidor en donde se tenga que desplegar la aplicación habrá que utilizar un archivo
u otro, e incluso cambiar algunos parámetros de configuración.  

A continuación escribimos los servidores en donde se ha probado el despliegue y ha funcionado correctamente con las configuraciones actuales, estos son:

* Tomcat
* Apache
* Weblogic 12.1.1
* Jboss Wildfly 8
* Jboss EAP 6.*

#### Tomcat y Apache
Las pruebas se han realizado sobre los servidores que te monta Eclipse de prueba y sobre los que Spring levanta la aplicación, por lo que puede darse el caso que cuando levantes la aplicación se realice directamente sobre _"http://localhost:8080"_, por lo que todo el mapeo de la aplicación se realizará sobre esta dirección.  

Si este es tu caso solo tienes que cambiar el valor de la variable **spring.contextPath** dentro del archivo de _application.properties_, ya que esta variable está pensada para los otros servidores cuando la dirección es: _"http://locahost:8080/java-api-rest"_.

#### Weblogic
Weblogic es otro de los servidores que utilizamos en el CDM, sobretodo para nuestro cliente de Red Abogacía, por lo que también hemos añadido esta opción.

Los archivos importantes para que funcione la aplicación en esta tipo de servidores es: _"/src/main/webapp/WEB-INF/weblogic.xml"_, ya que definimos las dependencias que no queremos que utilice del servidor y que coja directamente las de la aplicación, y por otro lado revisar que en **Application.java** está añadido _"implements WebApplicationInitializer"_.

#### Jboss Wildfly o Jboss EAP
En caso de que queramos montar la aplicación en un servidor Jboss Wildfly o Jboss EAP tenemos utilizar el archivo dentro del directorio: 
_"/src/main/webapp/WEB-INF/jboss-deployment-structure.xml"_, ya que como con Weblogic tenemos que definir las dependencias que queremos que utilice.

### Instalación
Como esta aplicación se ha desplegado sobre varios servidores os dejamos el ejemplo de como configurarla bajo un servidor JBoss WildFly 8. Y para ello primero tendremos que instalar JBoss Tools y luego configuraremos el WildFly:

* **JBoss Tools 4.3.0.Final**: [http://tools.jboss.org/downloads/](http://tools.jboss.org/downloads/)
Para instalar esta herramienta se puede hacer o a través del Market del propio Eclipse o instalarte su propia versión de Eclipse con todas las herramientas.

* **WildFly 8**: [https://docs.jboss.org/author/display/WFLY8/Getting+Started+Guide#GettingStartedGuide-Download](https://docs.jboss.org/author/display/WFLY8/Getting+Started+Guide#GettingStartedGuide-Download)

* **Tutorial configurar un servidor WildFly**:
[https://github.com/wildfly/quickstart/blob/10.x/guide/GettingStarted.asciidoc] (https://github.com/wildfly/quickstart/blob/10.x/guide/GettingStarted.asciidoc)

### Origen de datos
Para configurar la base de datos a la que accedemos desde la aplicación tendremos que cambiar la configuración que definimos en el archivo: **application.properties**.
Y cambiar el driver de la base de datos en el **pom.xml** de la aplicación.

En este caso hemos utilizado una base de datos MySQL configurada en el servidor del CDM, por lo que a continuación ponemos el código utilizado para la conexión.

**application.properties**

	spring.datasource.driverClassName=com.mysql.jdbc.Driver
	spring.datasource.url= jdbc:mysql://172.10.1.168:3306/java-api-rest-base
	spring.datasource.username=username
	spring.datasource.password=password
	
**pom.xml**

	<!-- Database Driver -->
	<!-- Here we have to define our Database driver, in this case we use MySQL -->
	<dependency>
		<groupId>mysql</groupId>
		<artifactId>mysql-connector-java</artifactId>
	</dependency>

## Logs
Para este proyecto hemos configurado un log sencillo para ir registrando todo lo que pasa en la aplicación, pero es posible que para proyectos más grandes se necesite una configuración más avanzada que por ejemplo vaya generando logs diarios.

En algunos servidores como Jboss tienen su propia implementación de logs, por lo que es posible que tengáis que configurarlos directamente en la consola de administración del servidor.

A continuación ponemos la configuración definida en el **application.properties**

	# ----------------------------------------
	# LOG4J
	# ----------------------------------------
	logging.level.org.springframework.web=INFO
	logging.level.org.hibernate=INFO
	logging.file=logs/java_api_base.log

## JUnit - Test
Es muy recomendable por no decir **obligatorio** el añadir a nuestros proyectos una pila de pruebas e integración continua, por lo que añadido una serie de test en la aplicación para tomarlos como ejemplo. Ya que luego a la hora de hacer la integración continua los va a ejecutar y de ellos de pende de que se realice la entrega o no.

## Integración continua - GitLab
Toda esta parte de integración continua la estamos configurando en nuestro GitLab, ya que hemos habilitado una serie de Runners, para que ejecute los comandos establecidos en el archivo **.gitlab-ci.xml**. Esto hará que cada vez que se haga un push a una rama importante se lancen los comandos de este archivos.

## Model Mapper
Esta librería de terceros es bastante fácil de usar y te ayuda bastante a la hora de realizar un mapeo entre dos objetos, por lo que con una serie de comandos podremos mapear un DTO a un Entity con una sola linea.

## Lombok
Esta otra librería te auto genera los _set_ y _get_ de las entidades, aunque para su correcto funcionamiento tendréis que añadir la librería cuando se inicie vuestro IDE.

## Sonar
Otra de las herramientas que utilizamos aquí en el CDM es Sonar, ya que nos ayuda a revisar nuestro código. Por lo que es recomendable que antes de hacer alguna subida o algún pase a producción, revisemos nuestro código. Para poder lanzar Sonar, solo tendremos que configurar un profile en el _"settings.xml"_ de nuestro directorio Maven. De manera que con un simple comando: _"mvn sonar"_, ya se conecta con la aplicación y realiza la comprobación del código.

## Javadoc
Al igual que es importante tener una serie de test en la aplicación, también es importante tener una buena documentación para que en caso de que cualquiera tenga que coger la aplicación pueda ver todas las funciones que hay. Por lo que para este ejemplo veréis que todos los comentarios están preparados para que se pueda generar la documentación mediante Javadoc.